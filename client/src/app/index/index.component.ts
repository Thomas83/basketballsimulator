import { Component, OnInit } from '@angular/core';
import { SecurityService } from '../security/security.service';
import { User } from '../security/user';


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  user: User|null = null;
  clubChoice: boolean = true; 

  constructor(private security: SecurityService) { }

  ngOnInit(): void {
    this.security.getUser().subscribe(
      user => this.user = user);
      console.log(this.user)

   
    
  }

}
