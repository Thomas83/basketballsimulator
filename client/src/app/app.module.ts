import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginFormComponent } from './security/login-form/login-form.component';
import { UserInfoComponent } from './security/user-info/user-info.component';
import { GreetingCreateComponent } from './greeting/greeting-create/greeting-create.component';
import { AppRoutingModule } from './app-routing.module';
import { StartseiteComponent } from './startseite/startseite.component';
import { RegisterComponent } from './security/register/register.component';
import { SidebarModule } from 'ng-sidebar';
import { SidebarComponent } from './sidebar/sidebar.component';
import { IndexComponent } from './index/index.component';
import { ClubSelectionComponent } from './clubs/club-selection/club-selection.component';
import { ClubDetailComponent } from './clubs/club-detail/club-detail.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatGridListModule} from '@angular/material/grid-list';
import { PlayersearchComponent } from './playersearch/playersearch.component';
import { PlayersearchresultComponent } from './playersearchresult/playersearchresult.component';
import {MatCardModule} from '@angular/material/card';




@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    UserInfoComponent,
    GreetingCreateComponent,
    StartseiteComponent,
    RegisterComponent,
    SidebarComponent,
    IndexComponent,
    ClubSelectionComponent,
    ClubDetailComponent,
    PlayersearchComponent,
    PlayersearchresultComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SidebarModule.forRoot(),
    MatGridListModule,
    MatCardModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
