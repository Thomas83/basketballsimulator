import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Club } from '../clubs/club';


@Injectable({
  providedIn: 'root'
})
export class ClubService {
  constructor(private http: HttpClient) { }

  getAllClubs() {
    return this.http.get<Club[]>('/api/clubs');
  }

  clubChoice(club: Club){
    return this.http.post('/api/clubchoice', club);
  }

}