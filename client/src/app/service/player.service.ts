import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PlayerSearchResult } from '../clubs/player';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private http: HttpClient) { }



search(name:string) {
  return this.http.get<PlayerSearchResult>('/api/player/'+encodeURIComponent(name));
}
}

