import { Component, OnInit, Input } from '@angular/core';
import { Club } from '../club';
import { Player } from 'src/app/clubs/player';
import { ClubService } from '../../service/club.service';


@Component({
  selector: 'app-club-detail',
  templateUrl: './club-detail.component.html',
  styleUrls: ['./club-detail.component.css']
})
export class ClubDetailComponent implements OnInit {

  @Input() club: Club|null = null;
  selectedClub: Club|null = null;
  playerList: Player[]|null = null;

  
  
  constructor(private clubservice: ClubService) { }

  ngOnInit() {
  
  }
  
  clubChoice(club: Club){
    this.clubservice.clubChoice(club).subscribe( club => {
      this.club = this.club
      console.log(this.club)
    });
     
    }
}



