import { Player } from "src/app/clubs/player";


export interface Club {
    id: string;
    clubname: string;
    city: string;
    stadion: string;
    owner: string; 
    conference: string; 
    division: string; 
    bigLogoUrl: string;
    smallLogoUrl: string;
    playerList: Player[];
    
}

