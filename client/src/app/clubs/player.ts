export interface Player{
    id: number;
    firstname: string;
    surName: string;
    nation: string;
    college: string; 
    jerseyNumber: number; 
    birthday: string;     
}


export interface PlayerSearchResult {
    results: Player[];
}