import { Component, OnInit } from '@angular/core';
import { Club } from '../club';
import { ClubService } from '../../service/club.service';
import { Player } from 'src/app/clubs/player';

@Component({
  selector: 'app-club-selection',
  templateUrl: './club-selection.component.html',
  styleUrls: ['./club-selection.component.css']
})
export class ClubSelectionComponent implements OnInit {
  selectedClub: Club|null = null;
  clubs: Club[]|null = null;
  atlanticClubs: Club[]|null = null;
  centralClubs: Club[]|null = null;
  southeastClubs: Club[]|null = null;
  northwestClubs: Club[]|null = null;
  southwestClubs: Club[]|null = null; 
  pacificClubs: Club[]|null = null;
  playerList: Player[]|null = null;
  clubPlayerList: Player[]|null = null;

  


  constructor(private clubservice: ClubService) { }

  ngOnInit() {
   this.getAllClubs();
  }
  
  onSelect(club: Club): void {
    this.selectedClub = club;
    this.clubPlayerList = this.selectedClub.playerList;

  }


 
  getAllClubs(): void {
      this.clubservice.getAllClubs().subscribe(
       clubs => { 
         this.clubs = clubs;
         this.atlanticClubs = [];
         this.centralClubs = [];
         this.southeastClubs = [];
         this.northwestClubs = [];
         this.pacificClubs = [];
         this.southwestClubs = [];


        for(const club of clubs){
          if(club.division === 'Atlantic Division'){
          this.atlanticClubs.push(club);
            }
          if(club.division === 'Central Division'){
          this.centralClubs.push(club);
              }
          if(club.division === 'Southeast Division'){
          this.southeastClubs.push(club);
              }
          if(club.division === 'Northwest Division'){
              this.northwestClubs.push(club);
              }
          if(club.division === 'Pacific Division'){
            this.pacificClubs.push(club);
             }
          if (club.division === 'Southwest Division'){
            this.southwestClubs.push(club)
             }
        }    
    });
  }
}