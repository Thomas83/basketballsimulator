import { Component, OnInit } from '@angular/core';
import { Player } from '../clubs/player';
import { PlayerService } from '../service/player.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-playersearchresult',
  templateUrl: './playersearchresult.component.html',
  styleUrls: ['./playersearchresult.component.css']
})
export class PlayersearchresultComponent implements OnInit {
  name: string = '';
  players: Player[] = [];

  player = {
    firstname: '',
    surName: '',
    birthday: '',
  }; 

  constructor(private playerservice: PlayerService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.name = this.activatedRoute.snapshot.paramMap.get("name") || '';
    this.search();
  }

 search() {
    this.playerservice.search(this.name).subscribe(
      data => {
        this.players = data.results;
        console.log(this.players)
      },
      error => console.log(error)
    )
  }
}
