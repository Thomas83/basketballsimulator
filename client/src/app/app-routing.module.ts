import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GreetingCreateComponent } from './greeting/greeting-create/greeting-create.component';
import { LoginFormComponent } from './security/login-form/login-form.component';
import { StartseiteComponent } from './startseite/startseite.component';
import { RegisterComponent } from './security/register/register.component';
import { IndexComponent } from './index/index.component';
import { PlayersearchComponent } from './playersearch/playersearch.component';
import { PlayersearchresultComponent } from './playersearchresult/playersearchresult.component';



const routes: Routes = [
  { path: 'start', component: StartseiteComponent },
  { path: 'greeting', component: GreetingCreateComponent },
  { path: 'login' , component: LoginFormComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'index', component: IndexComponent},
  { path: 'playersearch', component: PlayersearchComponent},
  { path: 'search/:name', component: PlayersearchresultComponent},
  { path: '**', redirectTo: '/start'},


];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ],

})
export class AppRoutingModule {}
