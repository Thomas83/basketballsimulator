import { Component, OnInit } from '@angular/core';
import { GreetingService } from './greeting/greeting.service';
import { Greeting } from './greeting/greeting';
import { SecurityService } from './security/security.service';
import { User } from './security/user';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'client';
  greetings: Greeting[]|null = null;
  user: User|null = null;
  showMenu: boolean = true;
  routerOutlet: boolean = false;
  private _opened: boolean = true;
  private _modeNum: number = 2;
  private _positionNum: number = 0;
  private _animate: boolean = true;
  private _autoCollapseHeight: number = 500;
  private _autoCollapseWidth: number = 500;

  private _MODES: Array<string> = ['over', 'push', 'slide'];
  private _POSITIONS: Array<string> = ['left', 'right', 'top', 'bottom'];

  private _toggleOpened(): void {
    this._opened = !this._opened;
  }

  private _toggleMode(): void {
    this._modeNum++;

    if (this._modeNum === this._MODES.length) {
      this._modeNum = 0;
    }
  }

  private _toggleAutoCollapseHeight(): void {
    this._autoCollapseHeight = this._autoCollapseHeight;
  }

  private _toggleAutoCollapseWidth(): void {
    this._autoCollapseWidth = this._autoCollapseWidth;
  }

  private _togglePosition(): void {
    this._positionNum++;

    if (this._positionNum === this._POSITIONS.length) {
      this._positionNum = 0;
    }
  }


  private _toggleAnimate(): void {
    this._animate = !this._animate;
  }

  private _onOpenStart(): void {
   
  }

  private _onOpened(): void {

  }

  private _onCloseStart(): void {
 
  }

  private _onClosed(): void {

  }

  

  constructor(private greetingService: GreetingService, private security: SecurityService, private router:Router) {

  }
  

  ngOnInit(): void {
    this.security.getUser().subscribe(
      user => this.user = user
    );  
      // No Sidebar 
    this.router.events.forEach((event) => {
      if (this.router.url == '/start' || this.router.url == '/login' || this.router.url == '/register') {
        this.showMenu = false; this.routerOutlet = true;
        } else {
          this.showMenu = true; this.routerOutlet = false;
        }
      })
}
private _toggleSidebar() {
  this._opened = !this._opened;
}

logout() {
  this.security.logout();
}
}
