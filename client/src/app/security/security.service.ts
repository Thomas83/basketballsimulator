import { Injectable } from '@angular/core';
import { User } from './user';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  private user$ = new BehaviorSubject<User|null>(null);

  constructor(private http: HttpClient) {
    this.http.get<User|null>("/api/sessionUser").subscribe(
      user => this.user$.next(user),
    )
  }

  public getUser(): Observable<User|null> {
    return this.user$;
  }

  public login(username: string, password: string): Observable<User> {

    const headers = new HttpHeaders({
        authorization : 'Basic ' + btoa(username + ':' + password),
        "X-Requested-With": 'XMLHttpRequest',
    });

    const login$ = this.http.get<User>('/api/sessionUser', {headers}).pipe(share());
    
    login$.subscribe(
      user => this.user$.next(user),
      err => this.user$.next(null),
    );

    return login$;
  }

  logout() {
    const logout$ = this.http.post('/api/logout', {}).pipe(share());
    
    logout$.subscribe(() => this.user$.next(null));

    return logout$;
  }
  
  registerUser(username: string, password: string , email: string) {
    const body: User = {
      username: username,
      email: email,
      password: password,
      ownClub: 0,
    };
    return this.http.post('/api/register', body);
  }
}


