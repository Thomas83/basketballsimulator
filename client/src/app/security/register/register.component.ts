import { Component, OnInit } from '@angular/core';
import { SecurityService } from '../security.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  error: string|null = null;
  username = '';
  password = '';
  email = '';

  constructor(private security: SecurityService) { }

  ngOnInit() {
  }

register() {
    this.security.registerUser(this.username, this.password, this.email).subscribe({
      
      complete: () => this.error = null,
      error: err => this.error = err,
  });

}
}
