import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../service/player.service';
import { Player } from '../clubs/player';

@Component({
  selector: 'app-playersearch',
  templateUrl: './playersearch.component.html',
  styleUrls: ['./playersearch.component.css']
})
export class PlayersearchComponent implements OnInit {
  name: string = '';
  players: Player[] = [];

  player = {
    firstname: '',
    surName: '',
    birthday: '',
  }; 


  constructor(private playerservice: PlayerService) { }

  ngOnInit() {
    
  }

 search() {
    this.playerservice.search(this.name).subscribe(
      data => {
        this.players = data.results;
      },
      error => console.log(error)
    )
  }

}
