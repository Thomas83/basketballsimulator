import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap, share } from 'rxjs/operators';
import { Greeting } from './greeting';

@Injectable({
  providedIn: 'root'
})
export class GreetingService {

  private shouldLoad = true;
  private greetings$ = new BehaviorSubject<Greeting[]>([]);

  constructor(private http: HttpClient) { }

  public getGreetings(): Observable<Greeting[]> {
    if (this.shouldLoad) {
      this.shouldLoad = false;
      this.http.get<Greeting[]>('/api/greeting').subscribe(
        greetigs => this.greetings$.next(greetigs)
      );
    }
    return this.greetings$;
  }

  public createGreeting(greeting: Greeting) {
    const result$ = this.http.post('/api/greeting', greeting).pipe(share());
    
    result$.subscribe(() => {
      // refresh the greetings
      this.shouldLoad = true;
      this.getGreetings();
    });

    return result$;
  }
}
