import { Component, OnInit } from '@angular/core';
import { GreetingService } from '../greeting.service';

@Component({
  selector: 'app-greeting-create',
  templateUrl: './greeting-create.component.html',
  styleUrls: ['./greeting-create.component.css']
})
export class GreetingCreateComponent implements OnInit {

  greeting = {
    text: ''
  };

  constructor(private greetingService: GreetingService) { }

  ngOnInit() {
  }

  submit() {
    this.greetingService.createGreeting(this.greeting).subscribe(() => {
      // reset form
      this.greeting.text = '';
    });
  }

}
