package com.example.server;

import com.example.server.game.Game;
import com.example.server.game.GameService;
import com.example.server.player.Player;
import com.example.server.player.PlayerRepository;
import com.example.server.player.PlayerSkill;
import com.example.server.player.PlayerStat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;


@RunWith(MockitoJUnitRunner.class)
public class GameServiceTest {

    @Spy
    private GameService spyGameService;

    @Mock
    private GameService mockGameService;

    @Autowired
    private GameService gameService;

    public Player buildPlayerForTest(){
        Player player = new Player();
        List<PlayerStat> playerStatsList = new ArrayList<>();
        playerStatsList.add(new PlayerStat());
        player.setPlayerStatList(playerStatsList);
        return player;
    }

    @Test
    public void setPlayerStatsTest(){
        List<Player> playerList = new ArrayList<>();
        playerList.add(new Player());
        Game game = mock(Game.class);
        playerList = spyGameService.setPlayerStats(game,playerList);
        assertEquals(game, playerList.get(0).getPlayerStatList().get(0).getGame());
    }
    @Test
    public void chanceTest(){
        int chance = spyGameService.chance();
        assertThat(chance, allOf(greaterThan(0),lessThan(100)));
    }
    @Test
    public void freeThrowTest_HIT(){
        Player player = buildPlayerForTest();
        PlayerSkill playerSkill = new PlayerSkill();
        playerSkill.setFreeThrow(100);
        player.setPlayerSkill(playerSkill);
        assertTrue(spyGameService.freeThrow(2,player));
        assertEquals(2,player.getPlayerStatList().get(0).getPoints());
    }
    @Test
    public void freeThrowTest_FAIL(){
        Player player = buildPlayerForTest();
        PlayerSkill playerSkill = new PlayerSkill();
        playerSkill.setFreeThrow(0);
        player.setPlayerSkill(playerSkill);
        assertFalse(spyGameService.freeThrow(3,player));
        assertEquals(0,player.getPlayerStatList().get(0).getPoints());
    }

    @Test
    public void free3PtTest_HIT(){
        Player player = buildPlayerForTest();
        PlayerSkill playerSkill = new PlayerSkill();
        playerSkill.setThreept(101);
        player.setPlayerSkill(playerSkill);
        assertTrue(spyGameService.free3pt(player));
    }
    @Test
    public void free3PtTest_FAIL(){
        Player player = buildPlayerForTest();
        PlayerSkill playerSkill = new PlayerSkill();
        playerSkill.setThreept(0);
        player.setPlayerSkill(playerSkill);
        assertFalse(spyGameService.free3pt(player));
    }
    @Test
    public void defend3PtTest_HIT(){
        Player offPlayer = buildPlayerForTest();
        Player defPlayer = buildPlayerForTest();
        PlayerSkill playerSkill = new PlayerSkill();
        playerSkill.setThreept(100);
        playerSkill.setDefIQ(0);
        offPlayer.setPlayerSkill(playerSkill);
        defPlayer.setPlayerSkill(playerSkill);
        assertTrue(spyGameService.defend3Pt(offPlayer,defPlayer));
    }
    @Test
    public void defend3PtTest_FAIL(){
        Player offPlayer = buildPlayerForTest();
        Player defPlayer = buildPlayerForTest();
        PlayerSkill playerSkill = new PlayerSkill();
        playerSkill.setThreept(0);
        playerSkill.setDefIQ(100);
        offPlayer.setPlayerSkill(playerSkill);
        defPlayer.setPlayerSkill(playerSkill);
        assertFalse(spyGameService.defend3Pt(offPlayer,defPlayer));
    }

    @Test
    public void hit3PtTest_STAT(){
        Player player = buildPlayerForTest();
        spyGameService.hit3Pt(player);
        assertEquals(3, player.getPlayerStatList().get(0).getPoints());
        assertEquals(1, player.getPlayerStatList().get(0).getTaken3pt());
        assertEquals(1,player.getPlayerStatList().get(0).getHit3pt());
    }
    @Test
    public void fail3PtTest_STAT(){
        Player player = buildPlayerForTest();
        spyGameService.fail3Pt(player);
        assertEquals(1, player.getPlayerStatList().get(0).getTaken3pt());
    }
    @Test
    public void hitFreeThrowTest_STAT(){
        Player player = buildPlayerForTest();
        spyGameService.hitFreeThrow(player);
        assertEquals(1, player.getPlayerStatList().get(0).getPoints());
        assertEquals(1, player.getPlayerStatList().get(0).getTakenFreeThrow());
        assertEquals(1,player.getPlayerStatList().get(0).getHitFreeThrow());
    }
    @Test
    public void failFreeThrowTest_STAT(){
        Player player = buildPlayerForTest();
        spyGameService.failFreeThrow(player);
        assertEquals(1, player.getPlayerStatList().get(0).getTakenFreeThrow());
    }
}
