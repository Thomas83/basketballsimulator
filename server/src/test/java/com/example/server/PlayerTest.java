package com.example.server;

import com.example.server.player.Player;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.Mockito.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

import static org.mockito.Mockito.mock;

public class PlayerTest {

    @Test
    public void getBirthdayTest(){
        Player player = new Player();
        player.setDayOfBirth(10);
        player.setMonthOfBirth(10);
        player.setYearOfBirth(2010);
        assertEquals("10.10.2010", player.getBirthday());
    }
    @Test
    public void getFirstPositionLongTest() {
        Player pg = new Player();
        pg.setFirstPosition("PG");
        assertEquals("Point Guard", pg.getFirstPositionLong());
        Player sg = new Player();
        sg.setFirstPosition("SG");
        assertEquals("Shooting Guard", sg.getFirstPositionLong());
        Player sf = new Player();
        sf.setFirstPosition("SF");
        assertEquals("Small Forward", sf.getFirstPositionLong());
        Player pf = new Player();
        pf.setFirstPosition("PF");
        assertEquals("Power Forward", pf.getFirstPositionLong());
        Player c = new Player();
        c.setFirstPosition("C");
        assertEquals("Center", c.getFirstPositionLong());
        Player error = new Player();
        error.setFirstPosition("What");
        assertEquals("Error", error.getFirstPositionLong());
    }
    @Test
    public void getSecondPositionLongTest() {
        Player pg = new Player();
        pg.setSecondPosition("PG");
        assertEquals("Point Guard", pg.getSecondPositionLong());
        Player sg = new Player();
        sg.setSecondPosition("SG");
        assertEquals("Shooting Guard", sg.getSecondPositionLong());
        Player sf = new Player();
        sf.setSecondPosition("SF");
        assertEquals("Small Forward", sf.getSecondPositionLong());
        Player pf = new Player();
        pf.setSecondPosition("PF");
        assertEquals("Power Forward", pf.getSecondPositionLong());
        Player c = new Player();
        c.setSecondPosition("C");
        assertEquals("Center", c.getSecondPositionLong());
        Player empty = new Player();
        empty.setSecondPosition("empty");
        assertEquals("", empty.getSecondPositionLong());
        Player error = new Player();
        error.setSecondPosition("What");
        assertEquals("Error", error.getSecondPositionLong());
    }
    @Test
    public void getFirstAndSecondPositionShortTest() {
        Player onlyFirstPosition = new Player();
        onlyFirstPosition.setFirstPosition("PG");
        onlyFirstPosition.setSecondPosition("empty");
        assertEquals("PG", onlyFirstPosition.getFirstAndSecondPositionShort());
        Player withSecondPosition = new Player();
        withSecondPosition.setFirstPosition("PF");
        withSecondPosition.setSecondPosition("PG");
        assertEquals("PF|PG", withSecondPosition.getFirstAndSecondPositionShort());
    }
}
