package com.example.server;

import com.example.server.clubs.Club;
import com.example.server.clubs.ClubRepository;
import com.example.server.player.Player;
import com.example.server.user.NewAccountService;
import com.example.server.user.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.xml.ws.Service;


import static org.assertj.core.api.AssertionsForClassTypes.useDefaultDateFormatsOnly;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NewAccountServiceTest{

    @Spy
    private NewAccountService spyNewAccountService;

    @Mock
    private NewAccountService mockedNewAccountService;

    @Mock
    private ClubRepository mockedClubRepository;

    public List<Club> buildMockedClubList() {
        List<Club> testClubList = mock(List.class);
        return testClubList;
    }

    public User buildMockedUserForTest(){
        User testUser = Mockito.mock(User.class);
        return testUser;
    }

    @Test
    public void buildClubsForNewUserTest_30Clubs(){
        List<Club> testClubList = spyNewAccountService.buildClubsForNewUser();
        assertThat(testClubList, hasSize(30));
    }

    @Test
    public void buildClubsForNewUserTest_15ClubsConference(){
        List<Club> testClubList = spyNewAccountService.buildClubsForNewUser();

        List<Club> easterConferenceClubList = new ArrayList<>();
        List<Club> westernConferenceClubList = new ArrayList<>();
        for(Club club : testClubList){
            if(club.getConference().equals("Eastern Conference")){
                easterConferenceClubList.add(club);
            }
            if(club.getConference().equals("Western Conference")){
                westernConferenceClubList.add(club);
            }
        }
        assertThat(easterConferenceClubList, hasSize(15));
        assertThat(westernConferenceClubList, hasSize(15));
    }

    @Test
    public void buildClubsForNewUserTest_5ClubsDivision(){
        List<Club> testClubList = spyNewAccountService.buildClubsForNewUser();
        List<Club> atlanticDivisionClubList = new ArrayList<>();
        List<Club> centralDivisionClubList = new ArrayList<>();
        List<Club> southeastDivisionClubList = new ArrayList<>();
        List<Club> northwestDivisionClubLst = new ArrayList<>();
        List<Club> pacificDivisionClubList = new ArrayList<>();
        List<Club> southwestDivisionClubList = new ArrayList<>();

        for(Club club : testClubList){
            if(club.getDivision().equals("Atlantic Division")){
                atlanticDivisionClubList.add(club); }
            if(club.getDivision().equals("Central Division")){
                centralDivisionClubList.add(club); }
            if(club.getDivision().equals("Southeast Division")){
                southeastDivisionClubList.add(club); }
            if(club.getDivision().equals("Northwest Division")){
                northwestDivisionClubLst.add(club); }
            if(club.getDivision().equals("Pacific Division")){
                pacificDivisionClubList.add(club); }
            if(club.getDivision().equals("Southwest Division")){
                southwestDivisionClubList.add(club); }
        }
        assertThat(atlanticDivisionClubList, hasSize(5));
        assertThat(centralDivisionClubList, hasSize(5));
        assertThat(southeastDivisionClubList, hasSize(5));
        assertThat(northwestDivisionClubLst, hasSize(5));
        assertThat(pacificDivisionClubList, hasSize(5));
        assertThat(southwestDivisionClubList, hasSize(5));
    }

    @Test
    public void setUserforClubsTest(){
        List<Club> testClubList = spyNewAccountService.buildClubsForNewUser();
        User mockedUser = buildMockedUserForTest();
        spyNewAccountService.setUserforClubs(testClubList,mockedUser);

        for(Club club : testClubList) {
            assertEquals(mockedUser, club.getUser());
        }
    }
}
