package com.example.server;

import com.example.server.clubs.Club;
import com.example.server.clubs.ClubRepository;
import com.example.server.game.Game;
import com.example.server.season.NewSeasonService;
import com.example.server.season.Season;
import com.example.server.season.SeasonRepository;
import com.example.server.user.NewAccountService;
import com.example.server.user.User;
import com.example.server.user.UserRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NewSeasonServiceTest {

    @Spy
    private NewAccountService spyNewAccountService;

    @Spy
    private NewSeasonService spyNewSeasonService;

    @Mock
    private SeasonRepository mockedSeasonRepository;

    @InjectMocks
    private NewSeasonService mockedNewSeasonService;

    @Mock
    private UserRepository mockedUserRepository;

    @Mock
    private ClubRepository mockedclubRepository;



    public List<Club> buildTestClubList() {
        List<Club> testClubList = spyNewAccountService.buildClubsForNewUser();
        for (int i = 0; i < testClubList.size(); i++) {
            Club club = testClubList.get(i);
            club.setId((long) i);       /// Anstatt ID von der Datenbank. Macht man das so ?!?
        }
        return testClubList;
    }

    public Season buildTestSeason2018(){
        int year = 2018;
        Season testSeason = spyNewSeasonService.createNewSeason(year);
        return testSeason;
    }

    public Season buildTestSeason2019(){
        int year = 2019;
        Season testSeason = spyNewSeasonService.createNewSeason(year);
        return testSeason;
    }

    @Ignore
    public void createYearForTheNewSeasonTest_NewUser(){
        User testUser = Mockito.mock(User.class);
        when(mockedSeasonRepository.findSeasonByUser(testUser)).thenReturn(null);
        int year = mockedNewSeasonService.createYearForNewSeason(testUser);
        assertThat(year, is(2018));
    }
    @Ignore
    public void createYearForTheNewSeasonTest_NextSeason(){
        User testUser = Mockito.mock(User.class);
        List<Season> seasonTestList = mock(ArrayList.class);
        when(mockedSeasonRepository.findSeasonByUser(testUser)).thenReturn(seasonTestList);
        when(spyNewSeasonService.getLastSeason(seasonTestList)).thenReturn(2018);
        int year = spyNewSeasonService.createYearForNewSeason(testUser);
        assertThat(year, is(2019));
    }

    @Test
    public void getLastSeasonTest(){
        List<Season> testSeasonList = new ArrayList<>();
        testSeasonList.add(buildTestSeason2018());
        testSeasonList.add(buildTestSeason2019());
        assertThat(mockedNewSeasonService.getLastSeason(testSeasonList), is(2019));
    }

    @Test
    public void createNoGameDateForTheSeasonTest2018(){
        List<LocalDate> noGameDates = mockedNewSeasonService.createNoGameDateForTheSeason(2018);
        assertThat(noGameDates, hasSize(6));
        for(LocalDate noGameDate : noGameDates){
           assertThat(noGameDate.getYear(), is(2019));
        }
    }

    @Test
    public void createNoGameDateForTheSeasonTest2019(){
        List<LocalDate> noGameDates = mockedNewSeasonService.createNoGameDateForTheSeason(2019);
        assertThat(noGameDates, hasSize(6));
        for(LocalDate noGameDate : noGameDates){
            assertThat(noGameDate.getYear(), is(2020));
        }
    }
    @Test
    public void createAvailableGameDatesTest(){
        List<LocalDate> noGameDates = spyNewSeasonService.createNoGameDateForTheSeason(2018);
        Season season = buildTestSeason2018();
        List<LocalDate> availableGameDates = spyNewSeasonService.createAvailableGameDates(season,noGameDates);
        assertThat(availableGameDates, hasSize(173));
    }

    @Test
    public void structureClubsByDivisionTest() {
        List<Club> clubList = spyNewAccountService.buildClubsForNewUser();
        List<Club> atlanticDivisionClubList = spyNewSeasonService.structureClubsByDivision(clubList, "Atlantic Division");
        List<Club> centralDivisionClubList = spyNewSeasonService.structureClubsByDivision(clubList, "Central Division");
        List<Club> southeastDivisionClubList = spyNewSeasonService.structureClubsByDivision(clubList, "Southeast Division");
        List<Club> northwestDivisionClubList = spyNewSeasonService.structureClubsByDivision(clubList, "Northwest Division");
        List<Club> pacificDivisionClubList = spyNewSeasonService.structureClubsByDivision(clubList, "Pacific Division");
        List<Club> southwestDivisionClubList = spyNewSeasonService.structureClubsByDivision(clubList, "Southwest Division");
        assertThat(atlanticDivisionClubList, hasSize(5));
        assertThat(centralDivisionClubList, hasSize(5));
        assertThat(southeastDivisionClubList, hasSize(5));
        assertThat(northwestDivisionClubList, hasSize(5));
        assertThat(pacificDivisionClubList, hasSize(5));
        assertThat(southwestDivisionClubList, hasSize(5));
    }
    @Test
    public void buildGamesForDivisionTest() {
        List<Club> clubList = buildTestClubList();
        Season season = buildTestSeason2018();
        List<Club> atlanticDivisionClubList = spyNewSeasonService.structureClubsByDivision(clubList, "Atlantic Division");
        List<Game> gameList = spyNewSeasonService.buildGamesForDivision(atlanticDivisionClubList, season);
        assertThat(gameList, hasSize(40));
        for (Game game : gameList) {
            assertThat(game.getHomeTeamID(), notNullValue());
            assertThat(game.getAwayTeamID(), notNullValue());
        }
    }
    @Ignore
    public void buildGamesforConferenceTest(){
        List<Club> clubList = buildTestClubList();
        Season season = buildTestSeason2018();
        List<Game> gameList = spyNewSeasonService.buildGamesforConference(clubList,season);
        assertThat(gameList, hasSize(540));

    }




    @Test
    public void createGamesEasternAgainWesternConferenceTest(){
        List<Club> testClubList = buildTestClubList();
        Season season = buildTestSeason2018();
        List<Game> gameList = spyNewSeasonService.createGamesEasternAgainWesternConference(testClubList,season);
        assertThat(gameList, hasSize(450));
        for(Game game : gameList){
            assertThat(game.getHomeTeamID(), notNullValue());
            assertThat(game.getAwayTeamID(), notNullValue());
            assertThat(game.getSeason(), is(notNullValue()));
        }
    }

    @Test
    public void createNewSeasonTest2018(){
        Season testSeason = buildTestSeason2018();
        assertThat(testSeason.getStartseason(), is(LocalDate.of(2018,10,16)));
        assertThat(testSeason.getEndseason(), is(LocalDate.of(2019,04,13)));
        assertThat(testSeason.getCurrentday(), is(LocalDate.of(2018,10,16)));
    }
    
    @Test
    public void createNewSeasonTest2019(){
        Season testSeason = buildTestSeason2019();
        assertThat(testSeason.getStartseason(), is(LocalDate.of(2019,10,16)));
        assertThat(testSeason.getEndseason(), is(LocalDate.of(2020,04,13)));
        assertThat(testSeason.getCurrentday(), is(LocalDate.of(2019,10,16)));
    }

    @Test
    public void createGameDescriptionTest(){
        Club homeClub = new Club();
        homeClub.setShortcut("BOS");
        Club awayClub = new Club();
        awayClub.setShortcut("LAL");
        assertEquals("LAL - @BOS", mockedNewSeasonService.createGameDescription(homeClub,awayClub));
    }
}




