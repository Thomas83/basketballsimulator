package com.example.server;
import static org.junit.Assert.*;

import com.example.server.player.Player;
import com.example.server.player.PlayerStat;
import org.junit.Test;


public class PlayerStatTest {

    @Test
    public void getQuoteFreeThrowTest(){
        PlayerStat playerStat = new PlayerStat();
        playerStat.setHitFreeThrow(9);
        playerStat.setTakenFreeThrow(10);
        assertEquals("9/10", playerStat.getQuoteFreeThrow());
    }

    @Test
    public void getQuoteFreeThrowWithQuoteTest(){
        PlayerStat playerStat = new PlayerStat();
        playerStat.setHitFreeThrow(9);
        playerStat.setTakenFreeThrow(10);
        assertEquals("90.0 %", playerStat.getQuoteFreeThrowWithQuote());
    }


    @Test
    public void getQuote3ptTest(){
        PlayerStat playerStat = new PlayerStat();
        playerStat.setHit3pt(40);
        playerStat.setTaken3pt(100);
        assertEquals("40/100", playerStat.getQuote3pt());
    }

    @Test
    public void getQuote3ptWithQuoteTest(){
        PlayerStat playerStat = new PlayerStat();
        playerStat.setHit3pt(40);
        playerStat.setTaken3pt(100);
        assertEquals("40.0 %", playerStat.getQuote3ptWithQuote());
    }
    @Test
    public void getQuoteFieldGoalTest(){
        PlayerStat playerStat = new PlayerStat();
        playerStat.setHitFieldGoals(40);
        playerStat.setTakenFieldGoals(100);
        assertEquals("40/100", playerStat.getQuoteFieldGoal());
    }
    @Test
    public void getQuoteFieldGoalWithQuoteTest(){
        PlayerStat playerStat = new PlayerStat();
        playerStat.setHitFieldGoals(40);
        playerStat.setTakenFieldGoals(100);
        assertEquals("40.0 %", playerStat.getQuoteFieldGoalWithQuote());
    }

}
