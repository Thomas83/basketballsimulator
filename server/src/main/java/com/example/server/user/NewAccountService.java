package com.example.server.user;
import com.example.server.api.SecurityService;
import com.example.server.clubs.Club;
import com.example.server.clubs.ClubRepository;
import com.example.server.game.GameRepository;
import com.example.server.player.Player;
import com.example.server.player.PlayerRepository;
import com.example.server.player.PlayerSkill;
import com.example.server.player.PlayerStat;
import com.example.server.season.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class NewAccountService {

    @Autowired
    private ClubRepository clubRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SeasonRepository seasonRepository;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private NewSeasonService newSeasonService;

    //    Create new Clubs and Players, if the User did login at the first time
    public void buildPlayersAndClubs(User user) {
        List<Club> clubList = buildClubsForNewUser();

        clubList = setUserforClubs(clubList,user);
        saveClubs(clubList);
        buildPlayersForEachTeam(clubList,user);
        newSeasonService.buildNewSeason(user, clubList);
    }
    public List<Club> buildClubsForNewUser(){
        List<Club> clubList = new ArrayList<>();
        //  Eastern Conference
        //  Atlantic Division
        Club nets = new Club("Brooklyn Nets", "BKN","New York City", "Barclays Center", "Michail Prochorow", "Eastern Conference", "Atlantic Division", "assets/knickslogosmall.png", "assets/netslogosmall.png");
        Club knicks = new Club("New York Knicks","NYK", "New York City", "Madison Square Garden", "James L. Dolan", "Eastern Conference", "Atlantic Division", "assets/knickslogosmall.png", "assets/knickslogosmall.png");
        Club philadelphia = new Club("Philadelphia 76ers", "PHI","Philadelphia", "Wells Fargo Center", "Joshua Harris, Adam Aron, David Blitzer, Jason Levien, Art Wrubel, Erick Thohir, Will Smith, Jada Pinkett Smith", "Eastern Conference", "Atlantic Division", "assets/76erssmalllogo.png", "assets/76erssmalllogo.png");
        Club raptors = new Club("Toronto Raptors", "TOR","Toronto", "Scotiabank Arena", "John I. Bitove", "Eastern Conference", "Atlantic Division", "assets/raptorslogosmall,png", "assets/raptorslogosmall.png");
        clubList.add(nets);clubList.add(raptors);clubList.add(knicks);clubList.add(philadelphia);

        Club boston = new Club("Boston Celtics", "BOS", "Boston", "TD Garden", "Wycliffe Grousbeck", "Eastern Conference", "Atlantic Division", "assets/bostonlogobig.png", "assets/bostonlogosmall.png");
        clubList.add(boston);

        //  Central Division
        Club bulls = new Club("Chicago Bulls", "CHI","Chicago", "United Center", "Jerry Reinsdorf", "Eastern Conference", "Central Division", "assets/bullslogobig.png", "assets/bullslogosmall.png");
        clubList.add(bulls);

        Club cavaliers = new Club("Cleveland Cavaliers", "CLE","Cleveland", "Quicken Loans Arena", "Dan Gilbert", "Eastern Conference", "Central Division", "assets/bullslogobig.png", "assets/cavalierslogosmall.png");
        Club pistons = new Club("Detroit Pistons","DET", "Detroit", "Little Caesars Arena", "Tom Gores", "Eastern Conference", "Central Division", "assets/bullslogobig.png", "assets/pistonslogosmall.png");
        Club pacers = new Club("Indiana Pacers","IND", "Indianapolis", "Bankers Life Fieldhouse", "Herb Simon", "Eastern Conference", "Central Division", "assets/bullslogobig.png", "assets/pacerslogosmall.png");
        Club bucks = new Club("Milwaukee Bucks", "MIL","Milwaukee", "Fiserv Forum", "Wesley Edens, Marc Lasry", "Eastern Conference", "Central Division", "assets/bullslogobig.png", "assets/buckslogosmall.png");
        clubList.add(cavaliers);clubList.add(pistons);clubList.add(pacers);clubList.add(bucks);

        //  Southeast Division
        Club hawks = new Club("Atlanta Hawks", "ATL", "Atlanta", "State Farm Arena", "Atlanta Spirit LLC", "Eastern Conference", "Southeast Division", "assets/bullslogobig.png", "assets/hawkslogosmall.png");
        Club hornets = new Club("Charlotte Hornets", "CHA","Charlotte", "Spectrum Center", "Michael Jordan", "Eastern Conference", "Southeast Division", "assets/hornetslogosmall.png", "assets/hornetslogosmall.png");
        Club miami = new Club("Miami Heat", "MIA","Miami", "AmericanAirlines Arena", "Micky Arison", "Eastern Conference", "Southeast Division", "assets/hornetslogosmall.png", "assets/hornetslogosmall.png");
        Club orlando = new Club("Orlando Magic", "ORL","Orlando", "Amway Center", "Richard DeVos", "Eastern Conference", "Southeast Division", "assets/hornetslogosmall.png", "assets/hornetslogosmall.png");
        Club wizards = new Club("Washington Wizards", "WAS","Washington", "Capital One Arena", "Ted Leonsis", "Eastern Conference", "Southeast Division", "assets/hornetslogosmall.png", "assets/hornetslogosmall.png");
        clubList.add(miami);clubList.add(orlando);clubList.add(wizards);clubList.add(hawks);clubList.add(hornets);

        //  Western Conference
        //  Northwest Division
        Club nuggets = new Club("Denver Nuggets", "DEN","Denver", "Pepsi Center", "Stan Kroenke", "Western Conference", "Northwest Division", "assets/denverlogosmall", "assets/denverlogosmall.png");
        Club timberwolves = new Club("Minnesota Timberwolves", "MIN","Minneapolis", "Target Center", "Glen Taylor", "Western Conference", "Northwest Division", "assets/hornetslogosmall.png", "assets/hornetslogosmall.png");
        Club oklahoma = new Club("Oklahoma City", "OKC","Oklahoma City", "Chesapeake Energy Arena", "Professional Basketball Club LLC", "Western Conference", "Northwest Division", "assets/thunderlogosmall.png", "assets/thunderlogosmall.png");
        Club blazers = new Club("Portland Trail Blazers", "POR","Portland", "Moda Center", "Paul Allen", "Western Conference", "Northwest Division", "assets/hornetslogosmall.png", "assets/hornetslogosmall.png");
        Club jazz = new Club("Utah Jazz","UTA", "Salt Lake City", "Vivint Smart Home Arena", "Gail Miller, Larry H. Miller Sports & Entertainment Group of Companies ", "Western Conference", "Northwest Division", "assets/hornetslogosmall.png", "assets/hornetslogosmall.png");
        clubList.add(timberwolves);clubList.add(blazers);clubList.add(jazz);clubList.add(oklahoma);clubList.add(nuggets);

        //  Pacific Division
        Club warriors = new Club("Golden State Warriors", "GSW","Oakland", "Oracle Arena", "Peter Guber, Joe Lacob", "Western Conference", "Pacific Division", "assets/warriorslogosmall.png", "assets/warriorslogosmall.png");
        Club clippers = new Club("Los Angeles Clippers","LAC", "Los Angeles", "Staples Center", "Steve Ballmer", "Western Conference", "Pacific Division", "assets/clipperslogosmall.png", "assets/clipperslogosmall.png");
        Club lakers = new Club("Los Angeles Lakers", "LAL","Los Angeles", "Staples Center", "Jeanie Buss", "Western Conference", "Pacific Division", "assets/lakerslogosmall.png", "assets/lakerslogosmall.png");
        Club suns = new Club("Phoenix Suns", "PHX","Phoenix", "Talking Stick Resort Arena", "Robert Sarver", "Western Conference", "Pacific Division", "assets/hornetslogosmall.png", "assets/sunslogosmall.png");
        Club kings = new Club("Sacramento Kings", "SAC","Sacramento", "Golden One Center", "Vivek Ranadive, Raj Bhathal, Mark Mastrov, Chris Kelly,Shaquille O'Neal", "Western Conference", "Pacific Division", "assets/hornetslogosmall.png", "assets/kingslogosmall.png");
        clubList.add(warriors);clubList.add(lakers);clubList.add(clippers);clubList.add(suns);clubList.add(kings);

        //  Southwest Division
        Club dallas = new Club("Dallas Mavericks", "DAL","Dallas", "American Airline Center", "Mark Cuban", "Western Conference", "Southwest Division", "assets/hornetslogosmall.png", "assets/dallaslogosmall.png");
        Club houston = new Club("Houston Rockets", "HOU","Houston", "Toyota Center", "Leslie Alexander", "Western Conference", "Southwest Division", "assets/hornetslogosmall.png", "assets/rocketslogosmall.png");
        Club memphis = new Club("Memphis Grizzlies ", "MEM","Memphis", "FedExForum", "Robert Pera", "Western Conference", "Southwest Division", "assets/hornetslogosmall.png", "assets/memphislogosmall.png");
        Club pelicans = new Club("New Orleans Pelicans","NOP", "New Orleans", "Smoothie King Center", "Gayle Benson", "Western Conference", "Southwest Division", "assets/hornetslogosmall.png", "assets/pelicanslogosmall.png");
        Club spurs = new Club("San Antonio Spurs", "SAS","San Antonio", "AT&T Center", "Peter M. Holt", "Western Conference", "Southwest Division", "assets/hornetslogosmall.png", "assets/spurslogosmall.png");
        clubList.add(memphis);clubList.add(houston);clubList.add(dallas);clubList.add(pelicans);clubList.add(spurs);
        return clubList;
    }

    // setUser for Clubs
    public List<Club> setUserforClubs(List<Club> clubList, User user) {
        for (Club club : clubList) {
            club.setUser(user);
        }
        return clubList;
    }

    // save clubs by User
    public void saveClubs(List<Club> clubList) {
        for (Club club : clubList) {
            clubRepository.save(club);
        }
    }

    public void setUserForPlayers(List<Player> playerList, User user, Club club){
        for (Player player : playerList) {
            player.setUser(user);
            player.setClub(club);
        }
    }
    // Save Players by Team and User
    public void savePlayers(List<Player> playerList, Club club) {
        for (Player player : playerList) {
            playerRepository.save(player);
        }
    }



    public void buildPlayersForEachTeam(List<Club> clubList, User user){
        for(Club club : clubList){
            if(club.getClubname().equals("Chicago Bulls")){
                buildPlayersForTheBulls(club,user);
            }
            if(club.getClubname().equals("Boston Celtics")){
                buildPlayersForTheCeltics(club,user);
            }
        }
    }

    // Finish
    public void buildPlayersForTheBulls(Club club, User user) {
        List<Player> bullsPlayerList = new ArrayList<>();
//        Player lauri = new Player(("Lauri", "Markkanen", "Finnland", "Arizona", 24, 22, 05, 1997))
//        bullsPlayerList.add(new Player);
//        bullsPlayerList.add(new Player("Rawle", "Alkins", "USA", "Arizona Wildcats", 20, 21, 10, 1997));
//        bullsPlayerList.add(new Player("Ryan", "Arcidiacono", "USA", "Villanova", 51, 26, 10, 1994));
//        bullsPlayerList.add(new Player("Blakeney", "Antonio", "USA", "LSU", 9, 4, 10, 1996));
//        bullsPlayerList.add(new Player("Carter", "Wendell", "USA", "Duke", 34, 16, 04, 1999));
//        bullsPlayerList.add(new Player("Kris", "Dunn", "USA", "Providence", 32, 18, 03, 1994));
//        bullsPlayerList.add(new Player("Cristiano", "Felício", "Brazilian", "none", 6, 7, 7, 1992));
//        bullsPlayerList.add(new Player("Shaquille", "Harrison", "USA", "Tulsa", 3, 6, 10, 1993));
//        bullsPlayerList.add(new Player("Justin", "Holiday", "USA", "Washington", 7, 05, 04, 1989));
//        bullsPlayerList.add(new Player("Chandler", "Hutchison", "USA", "Boise State", 15, 26, 04, 1996));
//
        Player laVine = new Player("Zachary", "LaVine", "USA", "UCLA", 8, 10, 03, 1995,6.5,185, "PG", "SG");
        laVine.setPlayerSkill(new PlayerSkill(89,97,87,72,80,79,86,38,89,31,65,49,57,41,51,80,44,85,85,98,71,97,70,86));
        bullsPlayerList.add(laVine);

//        bullsPlayerList.add(new Player("Robin", "Lopez", "USA", "Stanford", 42, 01, 04, 1988));
//        bullsPlayerList.add(new Player("Jabari", "Parker", "USA", "Duke", 2, 15, 03, 1995));
//        bullsPlayerList.add(new Player("Cameron", "Payne", "USA", "Murray State", 22, 8, 8, 1994));
//        bullsPlayerList.add(new Player("Bobby", "Ports", "USA", " Arkansas", 5, 10, 02, 1995));
//        bullsPlayerList.add(new Player("Tyler", "Ulis", "USA", "Kentucky", 0, 5, 1, 1996));
//        bullsPlayerList.add(new Player("Denzel", "Valentine", "USA", "Michigan State", 45, 16, 11, 1993));
        setUserForPlayers(bullsPlayerList,user, club);
        savePlayers(bullsPlayerList,club);
    }
    // In Work
    public void buildPlayersForTheCeltics(Club club, User user) {
        List<Player> bostonPlayerList = new ArrayList<>();
    //        bostonPlayerList.add(new Player("Jayson", "Tatum", "USA", "Duke", 0, 03, 03, 1998));
    //        bostonPlayerList.add(new Player("Gordon", "Hayward", "USA", "Butler", 20, 23, 3, 1990));
        setUserForPlayers(bostonPlayerList,user,club);
        savePlayers(bostonPlayerList, club);
    }
}

