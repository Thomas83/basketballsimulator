package com.example.server.user;
import com.example.server.season.Season;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type="uuid-char")
    private UUID id = UUID.randomUUID();

    @NotEmpty
    private String username;
    @JsonIgnore
    private String password;

    @NotEmpty
    private String email;

    private long ownClub;


    public User(String username, String password, String email, long ownClub) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.ownClub = ownClub;
    }

    public User() {}

    public UUID getId() {
        return id;
    }
    public void setId(UUID id){ this.id = id;}

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) { this.username = username;}

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) { this.password = password; }

    public String getEmail() { return email; }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getOwnClub() { return ownClub; }

    public void setOwnClub(long ownClub) { this.ownClub = ownClub; }

}
