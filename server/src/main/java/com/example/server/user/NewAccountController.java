package com.example.server.user;

import com.example.server.api.SecurityService;
import com.example.server.clubs.Club;
import com.example.server.clubs.ClubRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class NewAccountController {

    @Autowired
    private SecurityService securityService;

    @Autowired
    private NewAccountService newAccountService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ClubRepository clubRepository;

    @PostMapping("/clubchoice")
    public Optional<Club> clubchoice(@RequestBody Club club) throws Exception {
        Long clubid = club.getId();
        if (!securityService.getSessionUser().isPresent()) {
            throw new UsernameNotFoundException("User in not logged in");
        }
        User user = securityService.getSessionUser().orElse(null);
//        User user = userRepository.findByUsername("test").orElse(null);
        if (user.getOwnClub() == 0){

        user.setOwnClub(clubid);
        } else {
            throw new Exception("Du hast bereits einen Club ausgewählt");
        }
        userRepository.save(user);
        return clubRepository.findById(clubid);
    }
}
