package com.example.server.user;
import org.springframework.lang.Nullable;


public class UserDTO {
    public String username;
    public String password;
    public String email;


    private User user;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public @Nullable User getUser() { return user; }

    public void setUser(@Nullable User user) {
        this.user = user;
    }

}

