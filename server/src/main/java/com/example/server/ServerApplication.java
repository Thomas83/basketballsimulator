package com.example.server;


import com.example.server.clubs.ClubRepository;

import com.example.server.security.WebSecurityConfiguration;
import com.example.server.user.NewAccountService;
import com.example.server.user.User;
import com.example.server.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Import;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@Import(WebSecurityConfiguration.class)
public class ServerApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(ServerApplication.class, args);
	}

    @Autowired
    private UserRepository userRepository;

	@Autowired
    private ClubRepository clubRepository;

	@Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private NewAccountService newAccountService;

    @Override
    public void run(String... args) throws Exception {
        if(!userRepository.findByUsername("test").isPresent()) {
            User user = new User("test", passwordEncoder.encode("test123"),"test@test.de", 0);
            userRepository.save(user);
            User temp  = userRepository.findByUsername(user.getUsername())
                    .orElseThrow(() -> new UsernameNotFoundException("Not found"));
            newAccountService.buildPlayersAndClubs(temp);
        }


    }
}
