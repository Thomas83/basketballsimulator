package com.example.server.clubs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ClubController {

    @Autowired
    ClubRepository clubRepository;

    @GetMapping("/clubs")
    public List<Club> getAllClubs() {
        List<Club> clubList;
        clubList = clubRepository.findAll();
        return clubList;
    }
}
