package com.example.server.clubs;

import com.example.server.player.Player;
import com.example.server.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;


import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "club")
public class Club {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;
    private String clubname;
    private String shortcut;
    private String city;
    private String arena;
    private String owner;
    private String bigLogoUrl;
    private String smallLogoUrl;

    // Only for building a season and testing
    @Transient
    private int gameCount;
    @Transient
    private int homeCount;
    @Transient
    private int awayCount;

    @Lob
    private String history;

    @ManyToOne
    private User user;

    @JsonManagedReference
    @OneToMany(mappedBy = "club", cascade = CascadeType.ALL)
    private List<Player> playerList = new ArrayList<>();

    private String division;
    private String conference;

    @Transient // only needed to create a season;
    private List<LocalDate> availablePlayDate;

    @Transient // Testing, to build a perfekt season
    private List<Club> opponentList;

    @JsonIgnore
    @Transient // only needed to create a season;
    private int backToBackGamesCount;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getClubname() {
        return clubname;
    }

    public void setClubname(String clubname) {
        this.clubname = clubname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArena() {
        return arena;
    }

    public void setArena(String arena) {
        this.arena = arena;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getHistory() { return history; }

    public void setHistory(String history) { this.history = history; }

    public String getDivision() { return division; }

    public void setDivision(String division) { this.division = division; }

    public String getConference() { return conference; }

    public void setConference(String conference) { this.conference = conference; }

    public User getUser() { return user; }

    public void setUser(User user) { this.user = user; }

    public String getBigLogoUrl() { return bigLogoUrl; }

    public void setBigLogoUrl(String bigLogoUrl) { this.bigLogoUrl = bigLogoUrl; }

    public String getSmallLogoUrl() { return smallLogoUrl; }

    public void setSmallLogoUrl(String smallLogoUrl) { this.smallLogoUrl = smallLogoUrl; }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<Player> playerList) {
        this.playerList = playerList;
    }

    public List<LocalDate> getAvailablePlayDate() { return availablePlayDate; }

    public void setAvailablePlayDate(List<LocalDate> availablePlayDate) { this.availablePlayDate = availablePlayDate; }

    public int getBackToBackGamesCount() { return backToBackGamesCount; }

    public void setBackToBackGamesCount(int backToBackGamesCount) { this.backToBackGamesCount = backToBackGamesCount; }

    public int getGameCount() { return gameCount; }

    public void setGameCount(int gameCount) { this.gameCount = gameCount; }

    public int getHomeCount() { return homeCount; }

    public void setHomeCount(int homeCount) { this.homeCount = homeCount; }

    public int getAwayCount() { return awayCount; }

    public void setAwayCount(int awayCount) { this.awayCount = awayCount; }

    public List<Club> getOpponentList() { return opponentList; }

    public void setOpponentList(List<Club> opponentList) { this.opponentList = opponentList; }

    public String getShortcut() { return shortcut; }

    public void setShortcut(String shortcut) { this.shortcut = shortcut; }

    public Club(String clubname, String shortcut, String city, String arena, String owner, String conference, String division, String bigLogoUrl, String smallLogoUrl) {
        this.clubname = clubname;
        this.shortcut = shortcut;
        this.city = city;
        this.arena = arena;
        this.owner = owner;
        this.conference = conference;
        this.division = division;
        this.bigLogoUrl = bigLogoUrl;
        this.smallLogoUrl = smallLogoUrl;
    }

    public Club() {
    }
}

