package com.example.server.game;

import com.example.server.player.Player;
import com.example.server.player.PlayerRepository;
import com.example.server.player.PlayerSkill;
import com.example.server.player.PlayerStat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class GameService {

    @Autowired
    private PlayerRepository playerRepository;


    // build a game
    public void startGame(Game game){
        long homeID = game.getHomeTeamID();
        long awayID = game.getAwayTeamID();
        List<Player> homeTeam = getListofPlayer(homeID);
        List<Player> awayTeam = getListofPlayer(awayID);

    }

    public List<Player> getListofPlayer(Long id){
        List<Player> playerList = playerRepository.findByclubId(id);
        return playerList;

    }
    public List<Player> setPlayerStats(Game game, List<Player> playerList) {
        List<PlayerStat> playerStatsList = new ArrayList<>();
        PlayerStat playerStat = new PlayerStat();
        playerStat.setGame(game);
        playerStatsList.add(playerStat);
        for (Player player : playerList) {
            player.setPlayerStatList(playerStatsList);
        }
        return playerList;
    }

    //Game Logic
    public int chance(){
        int max = 100;
        int min = 0;
        int diff = max - min;
        Random randomValue = new Random();
        int chance = randomValue.nextInt(diff + 1);
        chance += min;
        return chance;
    }


    public boolean freeThrow(int number, Player player) {
        int freeThrowSkill = player.getPlayerSkill().getFreeThrow();
        int count = 0;
        for (int i = 0; i < number; i++) {
            int chance = chance();
            count++;
            if (100 < (chance + freeThrowSkill)) {
                hitFreeThrow(player);
                if (count == number) {
                    return true;
                }
            } else {
                failFreeThrow(player);
            }
        }
        return false;
    }




    public boolean defend3Pt(Player offPlayer, Player defPlayer) {
        int threePtSkill = offPlayer.getPlayerSkill().getThreept();
        int permiterDefSkill = defPlayer.getPlayerSkill().getDefIQ()/4*3;
        int chance = chance();
        if (100 < (chance + threePtSkill - permiterDefSkill)) {
            hit3Pt(offPlayer);
            return true;
        } else {
            fail3Pt(offPlayer);
        }
        return false;
    }

    public boolean free3pt(Player player) {
        int threePtSkill = player.getPlayerSkill().getThreept();
        int chance = chance();
        if (100 < (chance + threePtSkill)) {
            hit3Pt(player);
            return true;
        } else {
            fail3Pt(player);
        }
        return false;
    }

    // Stats
    public void hitFreeThrow(Player player){
        player.getPlayerStatList().get(0).setHitFreeThrow(player.getPlayerStatList().get(0).getHitFreeThrow()+1);
        player.getPlayerStatList().get(0).setTakenFreeThrow(player.getPlayerStatList().get(0).getTakenFreeThrow()+1);
        player.getPlayerStatList().get(0).setPoints(player.getPlayerStatList().get(0).getPoints()+1);
    }
    public void failFreeThrow(Player player){
        player.getPlayerStatList().get(0).setTakenFreeThrow(player.getPlayerStatList().get(0).getTakenFreeThrow()+1);
    }
    public void hit3Pt(Player player){
        player.getPlayerStatList().get(0).setHit3pt(player.getPlayerStatList().get(0).getHit3pt()+1);
        player.getPlayerStatList().get(0).setTaken3pt(player.getPlayerStatList().get(0).getTaken3pt()+1);
        player.getPlayerStatList().get(0).setPoints(player.getPlayerStatList().get(0).getPoints()+3);
    }
    public void fail3Pt(Player player){
        player.getPlayerStatList().get(0).setTaken3pt(player.getPlayerStatList().get(0).getTaken3pt()+1);
    }
}
