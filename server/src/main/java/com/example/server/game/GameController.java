package com.example.server.game;

import com.example.server.clubs.Club;
import com.example.server.clubs.ClubRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class GameController {

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private ClubRepository clubRepository;

    @GetMapping("/testseason")
    public List<Club> testSeason(){
        Long season = (long)1;
        List<Game> seasonlist = gameRepository.findAllGamesByseasonId(season);

        List<Club> clubList = clubRepository.findAll();
        for(Game game : seasonlist){
                long homeID = game.getHomeTeamID();
                long awayID = game.getAwayTeamID();
                for(Club club : clubList){
                    if(homeID == club.getId()){
                        club.setGameCount(club.getGameCount()+1);
                        club.setHomeCount(club.getHomeCount()+1);
                    }
                    if(awayID == club.getId()){
                        club.setGameCount(club.getGameCount()+1);
                        club.setAwayCount(club.getAwayCount()+1);
                    }
                }

        }
        List<Club> testList = new ArrayList<>();
        for(Club club : clubList){
            if(club.getGameCount()!=82){
                testList.add(club);
            }
        }
        return testList;
    }
}
