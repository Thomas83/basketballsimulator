package com.example.server.game;

import com.example.server.game.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends JpaRepository<Game, Long> {
    List<Game> findAllGamesByhomeTeamID(Long homeTeamID);
    List<Game> findAllGamesByawayTeamID(Long awayTeamID);
    List<Game> findAllGamesByseasonId(Long seasonId);
}
