package com.example.server.game;

import com.example.server.player.Player;
import com.example.server.player.PlayerStat;
import com.example.server.season.Season;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "games")
public class Game {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;

    private long homeTeamID;
    private long awayTeamID;
    private String gameDescription;
    private int resultHometeam;
    private int resultAwayteam;

    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="dd.MM.yyyy")
    private LocalDate gamedate;

    @JsonBackReference
    @JoinColumn(name = "seasonId")
    @ManyToOne
    private Season season;

    @JsonManagedReference
    @OneToMany(mappedBy = "game", cascade = CascadeType.ALL)
    private List<PlayerStat> statList = new ArrayList<>();

    public Game(long homeTeamID, long awayTeamID, LocalDate gamedate) {
        this.homeTeamID = homeTeamID;
        this.awayTeamID = awayTeamID;
        this.gamedate = gamedate;
    }

    public Game() {}

    public Season getSeason() { return season; }

    public void setSeason(Season season) { this.season = season; }

    public long getHomeTeamID() {
        return homeTeamID;
    }

    public void setHomeTeamID(long homeTeamID) {
        this.homeTeamID = homeTeamID;
    }

    public long getAwayTeamID() {
        return awayTeamID;
    }

    public void setAwayTeamID(long awayTeamID) {
        this.awayTeamID = awayTeamID;
    }

    public int getResultHometeam() {
        return resultHometeam;
    }

    public void setResultHometeam(int resultHometeam) {
        this.resultHometeam = resultHometeam;
    }

    public int getResultAwayteam() {
        return resultAwayteam;
    }

    public void setResultAwayteam(int resultAwayteam) {
        this.resultAwayteam = resultAwayteam;
    }

    public LocalDate getGamedate() { return gamedate; }

    public void setGamedate(LocalDate gamedate) { this.gamedate = gamedate; }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getGameDescription() { return gameDescription; }

    public void setGameDescription(String gameDescription) { this.gameDescription = gameDescription; }

    public List<PlayerStat> getStatList() { return statList; }

    public void setStatList(List<PlayerStat> statList) { this.statList = statList; }
}
