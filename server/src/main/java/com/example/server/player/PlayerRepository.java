package com.example.server.player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {
    List<Player> findByfirstName (String firstName);
    List<Player> findBysurName (String surName);
    List<Player> findByclubId(Long clubID);
 
}
