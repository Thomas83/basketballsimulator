package com.example.server.player;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.LinkedList;
import java.util.List;


@RestController
@RequestMapping("/api")
public class PlayerController {

    @Autowired
    private PlayerRepository playerRepository;


    @GetMapping("/allplayers")
    public List<Player> getAllPlayers() {
        List<Player> playerList;
        playerList = playerRepository.findAll();
        return playerList;
    }
    @GetMapping("/player/{playername}")
    public List<Player> getPlayerbyName(@PathVariable String playername) throws Exception {
        List<Player> playerList = new LinkedList<>();
        for (String name : playername.split(" ")) {
            List<Player> tempList = playerRepository.findByfirstName(playername);
            List<Player> tempList2 = playerRepository.findBysurName(playername);
            if (tempList.size() == 0 && tempList2.size() == 0) {
                throw new Exception("Sorry, kein Spieler unter diesen Namen vorhanden.");
            }
            playerList.addAll(tempList);
            playerList.addAll(tempList2);
        }
        return playerList;
    }
}
