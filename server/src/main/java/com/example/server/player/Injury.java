package com.example.server.player;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "injury")
public class Injury {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;

    private String injury;
    private LocalDate startInjury;
    private LocalDate endInjury;

    @JsonBackReference
    @JoinColumn(name = "playerId")
    @ManyToOne
    private Player player;
}
