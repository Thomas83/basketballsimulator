package com.example.server.player;
import com.example.server.clubs.Club;
import com.example.server.user.User;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "player")
public class Player {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String firstName;
    private String surName;
    private String nation;
    private String college;
    private int jerseyNumber;
    private String firstPosition;
    private String secondPosition;

    @Transient // only build by day, month and yearOfBirth for the front-end
    private String birthday;

    @Transient // build long version of position for front-end
    private String firstPositionLong;

    @Transient // build long version of position for front-end;
    private String secondPositionLong;

    @Transient // build both position for front-end
    private String firstAndSecondPositionShort;

    @JsonIgnore
    private int dayOfBirth;

    @JsonIgnore
    private int monthOfBirth;

    @JsonIgnore
    private int yearOfBirth;

    @ManyToOne
    private User user;

    @JsonManagedReference
    @OneToOne(mappedBy = "player", cascade = CascadeType.ALL)
    private PlayerSkill playerSkill;

    private double height;
    private int weight;

    @JsonBackReference
    @JoinColumn(name = "clubId")
    @ManyToOne
    private Club club;

    @JsonManagedReference
    @OneToMany(mappedBy = "player", cascade = CascadeType.ALL)
    private List<Contract> contractList = new ArrayList<>();

    @JsonManagedReference
    @OneToMany(mappedBy = "player", cascade = CascadeType.ALL)
    private List<Injury> injuredList = new ArrayList<>();

    @JsonManagedReference
    @OneToMany(mappedBy = "player", cascade = CascadeType.ALL)
    private List<PlayerStat> playerStatList = new ArrayList<>();

    public Player(String firstName, String surName, String nation, String college, int jerseyNumber, int dayOfBirth, int monthOfBirth, int yearOfBirth, double height, int weight, String firstPosition, String secondPosition) {
        this.firstName = firstName;
        this.surName = surName;
        this.nation = nation;
        this.college = college;
        this.jerseyNumber = jerseyNumber;
        this.dayOfBirth = dayOfBirth;
        this.monthOfBirth = monthOfBirth;
        this.yearOfBirth = yearOfBirth;
        this.height = height;
        this.weight = weight;
        this.firstPosition = firstPosition;
        this.secondPosition = secondPosition;
    }
    public Player(){
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getNation() { return nation; }

    public void setNation(String nation) { this.nation = nation; }

    public String getCollege() { return college; }

    public void setCollege(String college) { this.college = college; }

    public int getJerseyNumber() {
        return jerseyNumber;
    }

    public void setJerseyNumber(int jerseyNumber) {
        this.jerseyNumber = jerseyNumber;
    }

    public int getDayOfBirth() {
        return dayOfBirth;
    }

    public void setDayOfBirth(int dayOfBirth) {
        this.dayOfBirth = dayOfBirth;
    }

    public int getMonthOfBirth() {
        return monthOfBirth;
    }

    public void setMonthOfBirth(int monthOfBirth) {
        this.monthOfBirth = monthOfBirth;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public PlayerSkill getPlayerSkill() {
        return playerSkill;
    }

    public void setPlayerSkill(PlayerSkill playerSkill) {
        this.playerSkill = playerSkill;
    }

    public String getBirthday() {
        StringBuilder birthday = new StringBuilder();
        birthday.append(getDayOfBirth())
                .append(".")
                .append(getMonthOfBirth())
                .append(".")
                .append(getYearOfBirth());
        return birthday.toString();
    }

    public User getUser() { return user; }

    public void setUser(User user) { this.user = user; }

    public List<Contract> getContractList() {
        return contractList;
    }

    public void setContractList(List<Contract> contractList) {
        this.contractList = contractList;
    }

    public List<Injury> getInjuredList() { return injuredList; }

    public void setInjuredList(List<Injury> injuredList) { this.injuredList = injuredList; }

    public List<PlayerStat> getPlayerStatList() { return playerStatList; }

    public void setPlayerStatList(List<PlayerStat> playerStatList) { this.playerStatList = playerStatList; }

    public double getHeight() { return height; }

    public void setHeight(double height) { this.height = height; }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getFirstPosition() { return firstPosition; }

    public void setFirstPosition(String firstPosition) { this.firstPosition = firstPosition; }

    public String getSecondPosition() { return secondPosition; }

    public void setSecondPosition(String secondPosition) { this.secondPosition = secondPosition; }

    public String getFirstPositionLong() {
        if(getFirstPosition().equals("PG")){
            return "Point Guard"; }
        if(getFirstPosition().equals("SG")) {
            return "Shooting Guard"; }
        if(getFirstPosition().equals("SF")){
            return "Small Forward";}
        if(getFirstPosition().equals("PF")){
            return "Power Forward"; }
        if(getFirstPosition().equals("C")){
            return "Center"; }
        return "Error";
    }

    public String getSecondPositionLong() {
        if(getSecondPosition().equals("PG")){
            return "Point Guard"; }
        if(getSecondPosition().equals("SG")) {
            return "Shooting Guard"; }
        if(getSecondPosition().equals("SF")){
            return "Small Forward";}
        if(getSecondPosition().equals("PF")){
            return "Power Forward"; }
        if(getSecondPosition().equals("C")){
            return "Center"; }
        if(getSecondPosition().equals("empty"))
            return "";
        return "Error";
    }

    public String getFirstAndSecondPositionShort() {
        if(getSecondPosition().equals("empty")){
            return getFirstPosition(); }
        StringBuilder position = new StringBuilder();
        position.append(getFirstPosition())
                .append("|")
                .append(getSecondPosition());
        return position.toString();
    }
}
