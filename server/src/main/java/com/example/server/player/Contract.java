package com.example.server.player;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name = "contracts")
public class Contract {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;

    private String contract;
    private String clubName;
    private int startYear;
    private int endYear;
    private boolean teamOption;
    private boolean playerOption;
    private int lengthOfOption;
    private double currentPayment;
    private double paymentFirstYear;
    private double paymentSecondYear;
    private double paymentThirdYear;
    private double paymentFourthYear;
    private double paymentFifthYear;
    private double paymentSixtYear;

    @JsonBackReference
    @JoinColumn(name = "playerId")
    @ManyToOne
    private Player player;

    public Contract(String contract, int startYear, int endYear, boolean teamOption, boolean playerOption, int lengthOfOption, double currentPayment, double paymentFirstYear, double paymentSecondYear, double paymentThirdYear, double paymentFourthYear, double paymentFifthYear, double paymentSixtYear, Player player) {
        this.contract = contract;
        this.startYear = startYear;
        this.endYear = endYear;
        this.teamOption = teamOption;
        this.playerOption = playerOption;
        this.lengthOfOption = lengthOfOption;
        this.currentPayment = currentPayment;
        this.paymentFirstYear = paymentFirstYear;
        this.paymentSecondYear = paymentSecondYear;
        this.paymentThirdYear = paymentThirdYear;
        this.paymentFourthYear = paymentFourthYear;
        this.paymentFifthYear = paymentFifthYear;
        this.paymentSixtYear = paymentSixtYear;
        this.player = player;
    }

    public Contract(){
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public boolean isTeamOption() {
        return teamOption;
    }

    public void setTeamOption(boolean teamOption) {
        this.teamOption = teamOption;
    }

    public boolean isPlayerOption() {
        return playerOption;
    }

    public void setPlayerOption(boolean playerOption) {
        this.playerOption = playerOption;
    }

    public int getLengthOfOption() {
        return lengthOfOption;
    }

    public void setLengthOfOption(int lengthOfOption) {
        this.lengthOfOption = lengthOfOption;
    }

    public double getCurrentPayment() {
        return currentPayment;
    }

    public void setCurrentPayment(double currentPayment) {
        this.currentPayment = currentPayment;
    }

    public double getPaymentFirstYear() {
        return paymentFirstYear;
    }

    public void setPaymentFirstYear(double paymentFirstYear) {
        this.paymentFirstYear = paymentFirstYear;
    }

    public double getPaymentSecondYear() {
        return paymentSecondYear;
    }

    public void setPaymentSecondYear(double paymentSecondYear) {
        this.paymentSecondYear = paymentSecondYear;
    }

    public double getPaymentThirdYear() {
        return paymentThirdYear;
    }

    public void setPaymentThirdYear(double paymentThirdYear) {
        this.paymentThirdYear = paymentThirdYear;
    }

    public double getPaymentFourthYear() {
        return paymentFourthYear;
    }

    public void setPaymentFourthYear(double paymentFourthYear) {
        this.paymentFourthYear = paymentFourthYear;
    }

    public double getPaymentFifthYear() {
        return paymentFifthYear;
    }

    public void setPaymentFifthYear(double paymentFifthYear) {
        this.paymentFifthYear = paymentFifthYear;
    }

    public double getPaymentSixtYear() {
        return paymentSixtYear;
    }

    public void setPaymentSixtYear(double paymentSixtYear) {
        this.paymentSixtYear = paymentSixtYear;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }


}
