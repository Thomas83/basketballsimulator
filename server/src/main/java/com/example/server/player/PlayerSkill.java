package com.example.server.player;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name = "playerSkill")
public class PlayerSkill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @JsonBackReference
    @JoinColumn(name = "playerId")
    @OneToOne
    private Player player;

    // offense skills
    private int layups;
    private int dunks;
    private int freeThrow;
    private int midrange;
    private int threept;
    private int passing;
    private int ballhandling;
    private int postOffense;
    private int offIQ;
    private int offRebounding;


    // defense skills
    private int defIQ;
    private int postDefense;
    private int defRebounding;
    private int steals;
    private int blocks;
    private int commitment;

    // physical skills
    private int strength;
    private int acceleration;
    private int speed;
    private int stamina;
    private int lateralQuickness;
    private int vertical;
    private int reaction;
    private int potential;

    public PlayerSkill(int layups, int dunks, int freeThrow, int midrange, int threept, int passing, int ballhandling, int postOffense, int offIQ, int offRebounding, int defIQ, int postDefense, int defRebounding, int steals, int blocks, int commitment, int strength, int acceleration, int speed, int stamina, int lateralQuickness, int vertical, int reaction, int potential) {
        this.layups = layups;
        this.dunks = dunks;
        this.freeThrow = freeThrow;
        this.midrange = midrange;
        this.threept = threept;
        this.passing = passing;
        this.ballhandling = ballhandling;
        this.postOffense = postOffense;
        this.offIQ = offIQ;
        this.offRebounding = offRebounding;
        this.defIQ = defIQ;
        this.postDefense = postDefense;
        this.defRebounding = defRebounding;
        this.steals = steals;
        this.blocks = blocks;
        this.commitment = commitment;
        this.strength = strength;
        this.acceleration = acceleration;
        this.speed = speed;
        this.stamina = stamina;
        this.lateralQuickness = lateralQuickness;
        this.vertical = vertical;
        this.reaction = reaction;
        this.potential = potential;
    }
    public PlayerSkill(){}

    public Long getId() {
        return id;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getLayups() {
        return layups;
    }

    public void setLayups(int layups) {
        this.layups = layups;
    }

    public int getDunks() {
        return dunks;
    }

    public void setDunks(int dunks) {
        this.dunks = dunks;
    }

    public int getFreeThrow() {
        return freeThrow;
    }

    public void setFreeThrow(int freeThrow) {
        this.freeThrow = freeThrow;
    }

    public int getMidrange() {
        return midrange;
    }

    public void setMidrange(int midrange) {
        this.midrange = midrange;
    }

    public int getThreept() {
        return threept;
    }

    public void setThreept(int threept) {
        this.threept = threept;
    }

    public int getPassing() {
        return passing;
    }

    public void setPassing(int passing) {
        this.passing = passing;
    }

    public int getBallhandling() {
        return ballhandling;
    }

    public void setBallhandling(int ballhandling) {
        this.ballhandling = ballhandling;
    }

    public int getPostOffense() {
        return postOffense;
    }

    public void setPostOffense(int postOffense) {
        this.postOffense = postOffense;
    }

    public int getOffIQ() {
        return offIQ;
    }

    public void setOffIQ(int offIQ) {
        this.offIQ = offIQ;
    }

    public int getOffRebounding() {
        return offRebounding;
    }

    public void setOffRebounding(int offRebounding) {
        this.offRebounding = offRebounding;
    }

    public int getDefIQ() {
        return defIQ;
    }

    public void setDefIQ(int defIQ) {
        this.defIQ = defIQ;
    }

    public int getPostDefense() {
        return postDefense;
    }

    public void setPostDefense(int postDefense) {
        this.postDefense = postDefense;
    }

    public int getDefRebounding() {
        return defRebounding;
    }

    public void setDefRebounding(int defRebounding) {
        this.defRebounding = defRebounding;
    }

    public int getSteals() {
        return steals;
    }

    public void setSteals(int steals) {
        this.steals = steals;
    }

    public int getBlocks() {
        return blocks;
    }

    public void setBlocks(int blocks) {
        this.blocks = blocks;
    }

    public int getCommitment() {
        return commitment;
    }

    public void setCommitment(int commitment) {
        this.commitment = commitment;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(int acceleration) {
        this.acceleration = acceleration;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getStamina() {
        return stamina;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    public int getLateralQuickness() {
        return lateralQuickness;
    }

    public void setLateralQuickness(int lateralQuickness) {
        this.lateralQuickness = lateralQuickness;
    }

    public int getVertical() {
        return vertical;
    }

    public void setVertical(int vertical) {
        this.vertical = vertical;
    }

    public int getReaction() {
        return reaction;
    }

    public void setReaction(int reaction) {
        this.reaction = reaction;
    }

    public int getPotential() {
        return potential;
    }

    public void setPotential(int potential) {
        this.potential = potential;
    }
}


