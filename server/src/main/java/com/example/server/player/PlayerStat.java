package com.example.server.player;

import com.example.server.game.Game;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name = "playerStat")
public class PlayerStat {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonBackReference
    @JoinColumn(name = "playerId")
    @ManyToOne
    private Player player;

    @JsonBackReference
    @JoinColumn(name = "gameId")
    @ManyToOne
    private Game game;

    private int points;
    private int assists;
    private int rebounds;

    private int turnover;
    private int blocks;
    private int steals;

    private int minutes;
    private final static String DNP = "DNP";

    private int taken3pt;
    private int hit3pt;
    private int takenFreeThrow;
    private int hitFreeThrow;
    private int takenFieldGoals;
    private int hitFieldGoals;


    // Only for Stats, not for DB
    @Transient
    private String quote3pt;
    @Transient
    private String quote3ptWithQuote;
    @Transient
    private String quoteFreeThrow;
    @Transient
    private String quoteFreeThrowWithQuote;
    @Transient
    private String quoteFieldGoal;
    @Transient
    private String quoteFieldGoalWithQuote;

    public PlayerStat(int points, int assists, int rebounds, int turnover, int blocks, int steals, int minutes, int taken3pt, int hit3pt, int takenFreeThrow, int hitFreeThrow, int takenFieldGoals, int hitFieldGoals) {
        this.points = points;
        this.assists = assists;
        this.rebounds = rebounds;
        this.turnover = turnover;
        this.blocks = blocks;
        this.steals = steals;
        this.minutes = minutes;
        this.taken3pt = taken3pt;
        this.hit3pt = hit3pt;
        this.takenFreeThrow = takenFreeThrow;
        this.hitFreeThrow = hitFreeThrow;
        this.takenFieldGoals = takenFieldGoals;
        this.hitFieldGoals = hitFieldGoals;
    }

    public PlayerStat() {
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Player getPlayer() { return player; }

    public void setPlayer(Player player) { this.player = player; }

    public int getTaken3pt() { return taken3pt; }

    public void setTaken3pt(int taken3pt) { this.taken3pt = taken3pt; }

    public int getHit3pt() { return hit3pt; }

    public void setHit3pt(int hit3pt) { this.hit3pt = hit3pt; }

    public void setQuote3pt(String quote3pt) { this.quote3pt = quote3pt; }

    public String getQuote3pt() {
        StringBuilder quote3pt = new StringBuilder();
        quote3pt.append(getHit3pt())
                .append("/")
                .append(getTaken3pt());
        return quote3pt.toString();
    }

    public String getQuote3ptWithQuote() {
        StringBuilder quote3pt = new StringBuilder();
        double quote = getHit3pt() * 100 / getTaken3pt();
        quote3pt.append(quote)
                .append(" %");
        return quote3pt.toString();
    }

    public void setQuote3ptWithQuote(String quote3ptWithQuote) {
        this.quote3ptWithQuote = quote3ptWithQuote;
    }

    public int getTakenFreeThrow() {
        return takenFreeThrow;
    }

    public void setTakenFreeThrow(int takenFreeThrow) {
        this.takenFreeThrow = takenFreeThrow;
    }

    public int getHitFreeThrow() {
        return hitFreeThrow;
    }

    public void setHitFreeThrow(int hitFreeThrow) {
        this.hitFreeThrow = hitFreeThrow;
    }

    public String getQuoteFreeThrow() {
        StringBuilder quoteFreeThrow = new StringBuilder();
        quoteFreeThrow.append(getHitFreeThrow())
                .append("/")
                .append(getTakenFreeThrow());
        return quoteFreeThrow.toString();

    }

    public void setQuoteFreeThrow(String quoteFreeThrow) {
        this.quoteFreeThrow = quoteFreeThrow;
    }

    public String getQuoteFreeThrowWithQuote() {
        StringBuilder quoteFreeThrow = new StringBuilder();
        double quote = getHitFreeThrow() * 100 / getTakenFreeThrow();
        quoteFreeThrow.append(quote)
                .append(" %");
        return quoteFreeThrow.toString();
    }

    public void setQuoteFreeThrowWithQuote(String quoteFreeThrowWithQuote) {
        this.quoteFreeThrowWithQuote = quoteFreeThrowWithQuote;
    }

    public int getTakenFieldGoals() { return takenFieldGoals; }

    public void setTakenFieldGoals(int takenFieldGoals) { this.takenFieldGoals = takenFieldGoals; }

    public int getHitFieldGoals() { return hitFieldGoals; }

    public void setHitFieldGoals(int hitFieldGoals) { this.hitFieldGoals = hitFieldGoals; }

    public String getQuoteFieldGoal() {
        StringBuilder quoteFieldGoal = new StringBuilder();
        quoteFieldGoal.append(getHitFieldGoals())
                .append("/")
                .append(getTakenFieldGoals());
        return quoteFieldGoal.toString();
    }

    public void setQuoteFieldGoal(String quoteFieldGoal) { this.quoteFieldGoal = quoteFieldGoal; }

    public String getQuoteFieldGoalWithQuote() {
        StringBuilder quoteFieldGoal = new StringBuilder();
        double quote = getHitFieldGoals() * 100 / getTakenFieldGoals();
        quoteFieldGoal.append(quote)
                .append(" %");
        return quoteFieldGoal.toString();
    }

    public void setQuoteFieldGoalWithQuote(String quoteFieldGoalWithQuote) { this.quoteFieldGoalWithQuote = quoteFieldGoalWithQuote; }

    public Game getGame() { return game; }

    public void setGame(Game game) { this.game = game; }

    public int getPoints() { return points; }

    public void setPoints(int points) { this.points = points; }

    public int getAssists() { return assists; }

    public void setAssists(int assists) { this.assists = assists; }

    public int getRebounds() { return rebounds; }

    public void setRebounds(int rebounds) { this.rebounds = rebounds; }

    public int getTurnover() { return turnover; }

    public void setTurnover(int turnover) { this.turnover = turnover; }

    public int getBlocks() { return blocks; }

    public void setBlocks(int blocks) { this.blocks = blocks; }

    public int getSteals() { return steals; }

    public void setSteals(int steals) { this.steals = steals; }

    public int getMinutes() { return minutes; }

    public void setMinutes(int minutes) { this.minutes = minutes; }

    public static String getDNP() { return DNP; }


}
