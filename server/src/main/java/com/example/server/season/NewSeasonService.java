package com.example.server.season;
import com.example.server.clubs.Club;
import com.example.server.clubs.ClubRepository;
import com.example.server.game.Game;
import com.example.server.game.GameRepository;
import com.example.server.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
public class NewSeasonService {

    @Autowired
    private SeasonRepository seasonRepository;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private ClubRepository clubRepository;

    //    Each team has to play:
    //
    //  4 games against the other 4 division opponents (4×4=16 games),
    //  4 games* against 6 (out-of-division) conference opponents (4×6=24 games),
    //  3 games against the remaining 4 conference teams (3×4=12 games),
    //  2 games against teams in the opposing conference (2×15=30 games).

    // Create a new Season for the User
    public void buildNewSeason(User user, List<Club> clubList) {
        int year = createYearForNewSeason(user);
        Season season = createNewSeason(year);
        season.setUser(user);
        season.setNoGameDates(createNoGameDateForTheSeason(year));

        // Create available game dates for each Club + set max count for Back to Back Games
        for (Club club : clubList) {
            List<LocalDate> availablePlayDate = createAvailableGameDates(season, season.getNoGameDates());
            List<Club> opponentList = new ArrayList<>();
            club.setAvailablePlayDate(availablePlayDate);
            club.setBackToBackGamesCount(15);
            club.setOpponentList(opponentList);
        }

        List<Game> gameList = createGamesList(clubList, season);
        setGameDateForGameList(clubList, gameList, season);
        gameList = buildGameDescription(gameList);
        season.setGameList(gameList);
        seasonRepository.save(season);
    }

    public Season createNewSeason(int year){
        LocalDate startseason = LocalDate.of(year, 10, 16);
        LocalDate endseason = LocalDate.of(year + 1, 04, 13);
        LocalDate currentday = LocalDate.of(year, 10, 16);
        Season season = new Season(year, startseason, endseason, currentday);
        return season;
    }

    // set a year for the season
    public int createYearForNewSeason(User user) {
        if (seasonRepository.findSeasonByUser(user).isEmpty()) {
            int year = 2018;
            return year;
        } else {
            List<Season> allSeasonsByUser = seasonRepository.findSeasonByUser(user);
            int year = getLastSeason(allSeasonsByUser)+1;
            return year;
        }
    }
    public int getLastSeason(List<Season> allSeasonsByUser){
        int lastEntry = allSeasonsByUser.size() - 1;
        Season lastSeason = allSeasonsByUser.get(lastEntry);
        int year = lastSeason.getYear();
        return year;
    }

    // sets no game dates for a season
    public List<LocalDate> createNoGameDateForTheSeason(int year) {
        List<LocalDate> noGameDates = new ArrayList<>();
        noGameDates.add(LocalDate.of(year + 1, 02, 16));
        noGameDates.add(LocalDate.of(year + 1, 02, 17));
        noGameDates.add(LocalDate.of(year + 1, 02, 18));
        noGameDates.add(LocalDate.of(year + 1, 02, 19));
        noGameDates.add(LocalDate.of(year + 1, 02, 20));
        noGameDates.add(LocalDate.of(year + 1, 02, 21));
        return noGameDates;
    }

    // create a List of available date for each team in the season
    public List<LocalDate> createAvailableGameDates(Season season, List<LocalDate> noGameDates) {
        int days = (int) ChronoUnit.DAYS.between(season.getStartseason(), season.getEndseason());
        List<LocalDate> availablePlayDates = new ArrayList<>();
        for (int i = 0; i < days; i++) {
            availablePlayDates.add(season.getStartseason().plusDays(i));
        }
        // removes all unavailable dates
        for (int i = 0; i < noGameDates.size(); i++) {
            LocalDate noGameDate = noGameDates.get(i);
            for (int j = 0; j < availablePlayDates.size(); j++) {
                LocalDate possibleDate = availablePlayDates.get(j);
                if (possibleDate.equals(noGameDate)) {
                    availablePlayDates.remove(possibleDate);
                    j++;
                }
            }
        }
        return availablePlayDates;
    }

    // structure the clubs for division and conference to build a list of games
    private List<Game> createGamesList(List<Club> clubList, Season season) {
        List<Game> easternVsWesternGameList = createGamesEasternAgainWesternConference(clubList, season);
        List<Game> ConferenceGameList = buildGamesforConference(clubList, season);

        List<Game> atlanticDivisionGameList = buildGamesForDivision(structureClubsByDivision(clubList, "Atlantic Division"), season);
        List<Game> centralDivisionGameList = buildGamesForDivision(structureClubsByDivision(clubList, "Central Division"), season);
        List<Game> southeastDivisionGameList = buildGamesForDivision(structureClubsByDivision(clubList, "Southeast Division"), season);
        List<Game> northwestDivisionGameList = buildGamesForDivision(structureClubsByDivision(clubList, "Northwest Division"), season);
        List<Game> pacificDivisionGameList = buildGamesForDivision(structureClubsByDivision(clubList, "Pacific Division"), season);
        List<Game> southwestDivisionGameList = buildGamesForDivision(structureClubsByDivision(clubList, "Southwest Division"), season);

        List<Game> gameList = Stream.of(easternVsWesternGameList, ConferenceGameList,
                atlanticDivisionGameList, centralDivisionGameList, southeastDivisionGameList, northwestDivisionGameList,
                pacificDivisionGameList, southwestDivisionGameList)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
        Collections.shuffle(gameList);

        return gameList;
    }

    // Structure clubs by division
    public List<Club> structureClubsByDivision(List<Club> clubList, String league) {
        List<Club> clubListByDivision = new ArrayList<>();
        for (Club club : clubList) {
            if (club.getDivision().equals(league)) {
                clubListByDivision.add(club);
            }
        }
        return clubListByDivision;
    }

    // Build a list of games for every Division
    public List<Game> buildGamesForDivision(List<Club> clubList, Season season) {
        List<Game> gameList = new ArrayList<>();
        for (int k = 0; k < 1; k++) {
            // Hometeam against Division Clubs two times
            for (int i = 0; i < clubList.size(); i++) {
                for (int j = 0; j < clubList.size(); j++) {
                    Game game = new Game();
                    game.setHomeTeamID(clubList.get(i).getId());
                    game.setAwayTeamID(clubList.get(j).getId());
                    if (game.getHomeTeamID() != game.getAwayTeamID()) {
                        gameList.add(game);
                        game.setSeason(season);
                    }
                }
            }
            // Awayteam against Division Clubs two times
            for (int l = 0; l < clubList.size(); l++) {
                for (int j = 0; j < clubList.size(); j++) {
                    Game game = new Game();
                    game.setHomeTeamID(clubList.get(j).getId());
                    game.setAwayTeamID(clubList.get(l).getId());
                    if (game.getHomeTeamID() != game.getAwayTeamID()) {
                        gameList.add(game);
                        game.setSeason(season);
                    }
                }
            }
        }
        return gameList;
    }

    // Build a list of games for every Conference, 36 games, 18 home, 18 away
    public List<Game> buildGamesforConference(List<Club> clubList, Season season) {
        List<Game> gameList = new ArrayList<>();
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < clubList.size(); i++) {
                Club homeClub = clubList.get(i);
                for (int j = 0; j < clubList.size(); j++) {
                    Club awayClub = clubList.get(j);
                    if (!homeClub.getDivision().equals(awayClub.getDivision()) && homeClub.getConference().equals(awayClub.getConference())) {
                        Game game = new Game();
                        game.setHomeTeamID(clubList.get(i).getId());
                        game.setAwayTeamID(clubList.get(j).getId());
                        if (game.getHomeTeamID() != game.getAwayTeamID()) {
                            gameList.add(game);
                            homeClub.setGameCount(homeClub.getGameCount() + 1);
                            awayClub.setGameCount(awayClub.getGameCount() + 1);
                            homeClub.setHomeCount(homeClub.getHomeCount() + 1);
                            awayClub.setAwayCount(awayClub.getAwayCount() + 1);
                            game.setSeason(season);
                        }
                    }
                }
            }
        }
        Collections.shuffle(gameList);
        gameList = removeGames(gameList, clubList);

        return gameList;
    }

    public List<Game> createGamesEasternAgainWesternConference(List<Club> clubList, Season season) {
        List<Game> gameList = new ArrayList<>();
        List<Club> easternClubList = new ArrayList<>();
        List<Club> westernClubList = new ArrayList<>();
        for (Club club : clubList) {
            if (club.getConference().equals("Eastern Conference")) {
                easternClubList.add(club);
            }
            if (club.getConference().equals("Western Conference")) {
                westernClubList.add(club);
            }
        }
        // HomeTeam
        for (int i = 0; i < easternClubList.size(); i++) {
            for (int j = 0; j < westernClubList.size(); j++) {
                Game game = new Game();
                game.setHomeTeamID(easternClubList.get(i).getId());
                game.setAwayTeamID(westernClubList.get(j).getId());
                if (game.getHomeTeamID() != game.getAwayTeamID()) {
                    gameList.add(game);
                    game.setSeason(season);
                }
            }
        }
        // AwayTeam
        for (int i = 0; i < westernClubList.size(); i++) {
            for (int j = 0; j < easternClubList.size(); j++) {
                Game game = new Game();
                game.setHomeTeamID(westernClubList.get(i).getId());
                game.setAwayTeamID(easternClubList.get(j).getId());
                if (game.getHomeTeamID() != game.getAwayTeamID()) {
                    gameList.add(game);
                    game.setSeason(season);
                }
            }
        }
        return gameList;
    }

    private List<Game> setGameDateForGameList(List<Club> clubList, List<Game> gameList, Season season) {
        boolean findGameDate = true;

        for (Game game : gameList) {
            int homeTeamID = (int) game.getHomeTeamID();
            int awayTeamID = (int) game.getAwayTeamID();
            Club homeClub = clubList.get(homeTeamID - 1);
            Club awayClub = clubList.get(awayTeamID - 1);
            List<LocalDate> availableDatesForHomeTeam = homeClub.getAvailablePlayDate();
            for (LocalDate availableDateHomeTeam : availableDatesForHomeTeam) {
                List<LocalDate> availableDatesForAwayTeam = awayClub.getAvailablePlayDate();
                for (int i = 0; i < availableDatesForAwayTeam.size(); i++) {
                    LocalDate availableDateAwayTeam = availableDatesForAwayTeam.get(i);
                    if (availableDateHomeTeam.equals(availableDateAwayTeam)) {
                        game.setGamedate(availableDateHomeTeam);
                        findGameDate = false;
                        homeClub.getAvailablePlayDate().remove(availableDateHomeTeam);
                        awayClub.getAvailablePlayDate().remove(availableDateHomeTeam);

                        // Give the Team that have a Back to Back game one day break
                        LocalDate lastDayOfAllStarBreak = season.getNoGameDates().get(season.getNoGameDates().size() - 1);
                        if (!availableDateHomeTeam.equals(season.getStartseason()) || !availableDateHomeTeam.equals(lastDayOfAllStarBreak)) {
                            for (LocalDate checkDayForHomeTeam : availableDatesForHomeTeam) {
                                if (checkDayForHomeTeam.equals(availableDateHomeTeam.minusDays(1))) {
                                    homeClub.getAvailablePlayDate().remove(availableDateHomeTeam.plusDays(1));
                                    break;
                                }
                            }
                            for (LocalDate checkDayForAwayTeam : availableDatesForAwayTeam) {
                                if (checkDayForAwayTeam.equals(availableDateHomeTeam.minusDays(1))) {
                                    awayClub.getAvailablePlayDate().remove(availableDateHomeTeam.plusDays(1));
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
                if (!findGameDate) {
                    findGameDate = true;
                    break;
                }
            }
        }
        return gameList;
    }

    public List<Game> removeGames(List<Game> gameList, List<Club> clubList) {
        List<Game> deleteGameList = new ArrayList<>();
        for (int i = 0; i < gameList.size(); i++) {
            if (gameList.size() == 540) {
                break;
            } else {
                int homeTeamID = (int) gameList.get(i).getHomeTeamID();
                int awayTeamID = (int) gameList.get(i).getAwayTeamID();
                Club homeClub = clubList.get(homeTeamID - 1);    // Muss noch gefixt werden !!
                Club awayClub = clubList.get(awayTeamID - 1);    // Muss noch gefixt werden !!
                if (homeClub.getOpponentList().size() != 4 && awayClub.getOpponentList().size() != 4
                        && homeClub.getHomeCount() != 18 && awayClub.getAwayCount() != 18) {
                    boolean possibleOpponent = checkOpponent(homeClub, awayClub);

                    if (possibleOpponent == true) {
                        List<Club> homeClubList = homeClub.getOpponentList();
                        List<Club> awayClubList = awayClub.getOpponentList();
                        homeClubList.add(awayClub);
                        awayClubList.add(homeClub);
                        homeClub.setOpponentList(homeClubList);
                        awayClub.setOpponentList(awayClubList);
                        deleteGameList.add(gameList.get(i));
                        gameList.remove(gameList.get(i));
                        homeClub.setGameCount(homeClub.getGameCount() - 1);
                        awayClub.setGameCount(awayClub.getGameCount() - 1);
                        homeClub.setHomeCount(homeClub.getHomeCount() - 1);
                        awayClub.setAwayCount(awayClub.getAwayCount() - 1);
                    }
                }
            }

        }
        if(gameList.size() != 540){
            restartRemoveGames(clubList,gameList,deleteGameList);
        }

        return gameList;
    }

    private boolean checkOpponent(Club homeClub, Club awayClub){
        for(Club club : homeClub.getOpponentList()){
            if(club.equals(awayClub)){
                return false;
            }
        }
        return true;
    }

    public void restartRemoveGames (List<Club> clubList, List<Game> gameList, List<Game> deleteGameList){
        for(Game game : deleteGameList){
            gameList.add(game);
        }
        for(Club club : clubList){
            List<Club> opponentList = new ArrayList<>();
            club.setOpponentList(opponentList);
            club.setHomeCount(20);
            club.setAwayCount(20);
        }
        Collections.shuffle(gameList);
        removeGames(gameList,clubList);
    }
    public List<Game> buildGameDescription(List<Game> gameList){
        for(Game game : gameList){
            Club homeClub = findClubByID(game.getHomeTeamID());
            Club awayClub = findClubByID(game.getAwayTeamID());
            String gameDescription = createGameDescription(homeClub,awayClub);
            game.setGameDescription(gameDescription);
        }
        return gameList;
    }

    public Club findClubByID(Long id){
        Club club = clubRepository.findById(id).orElse(null);
        return club;
    }



    public String createGameDescription(Club homeClub, Club awayClub){
        StringBuilder gameDescription = new StringBuilder();
        gameDescription.append(awayClub.getShortcut())
                .append(" - @")
                .append(homeClub.getShortcut());
        return gameDescription.toString();
    }
}


