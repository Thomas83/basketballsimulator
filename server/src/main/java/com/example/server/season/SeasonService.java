package com.example.server.season;

import com.example.server.game.Game;
import org.springframework.stereotype.Service;

import java.util.Collections;

import java.util.Comparator;
import java.util.List;

@Service
public class SeasonService {

    public List<Game> sortGameListbyDate(List<Game> gameList){
        Collections.sort(gameList, new Comparator<Game>() {
            public int compare(Game game1, Game game2) {
                return game1.getGamedate().compareTo(game2.getGamedate());
            }
        });
        return gameList;
    }
}
