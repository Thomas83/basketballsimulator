package com.example.server.season;

import com.example.server.api.SecurityService;
import com.example.server.game.Game;
import com.example.server.game.GameRepository;
import com.example.server.user.User;
import com.example.server.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class SeasonController {
    @Autowired
    private SeasonRepository seasonRepository;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SeasonService seasonService;


    @GetMapping("/allseasons")
    public List<Season> getAllSeasons() {
        List<Season> seasonlist = seasonRepository.findAll();
        return seasonlist;
    }

    @GetMapping("/wholecalender")
    public List<Game> getWholeCalender(){
//        if (!securityService.getSessionUser().isPresent()) {
//            throw new UsernameNotFoundException("User in not logged in");
//        }
//        User user = securityService.getSessionUser().orElse(null);
        User user = userRepository.findByUsername("test")
                .orElseThrow(() -> new UsernameNotFoundException("Not found"));

        List<Season> seasonList = seasonRepository.findSeasonByUser(user);
        Season season = seasonList.get(seasonList.size()-1);
        List<Game> gameList = gameRepository.findAllGamesByseasonId(season.getId());
        gameList = seasonService.sortGameListbyDate(gameList);
        return gameList;
    }

    @GetMapping("/owncalender")
    public List<Game> getOwnCalender(){
        User user = userRepository.findByUsername("test")
                .orElseThrow(() -> new UsernameNotFoundException("Not found"));

        List<Game> homeTeamList = gameRepository.findAllGamesByhomeTeamID(user.getOwnClub());
        List<Game> awayTeamList = gameRepository.findAllGamesByawayTeamID(user.getOwnClub());
        List<Game> gameList = Stream.of(homeTeamList,awayTeamList)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        gameList = seasonService.sortGameListbyDate(gameList);


        return gameList;
    }




}
