package com.example.server.season;
import com.example.server.game.Game;
import com.example.server.user.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;


import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "season")
public class Season {


    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    private int year;
    @JsonIgnore
    private LocalDate startseason;
    @JsonIgnore
    private LocalDate  endseason;

    @Transient
    private String currentseason;

    @Transient
    private List<LocalDate> noGameDates;

    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="dd.MM.yyyy")
    private LocalDate  currentday;

    @ManyToOne
    private User user;

    // List of all games in a season
    @JsonManagedReference
    @OneToMany(mappedBy = "season", cascade = CascadeType.ALL)
    private List<Game> gameList = new ArrayList<>();

    public int getYear() { return year; }

    public void setYear(int year) {
        this.year = year;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Game> getGameList() {
        return gameList;
    }

    public void setGameList(List<Game> gameList) {
        this.gameList = gameList;
    }

    public LocalDate getStartseason() { return startseason; }

    public void setStartseason(LocalDate startseason) { this.startseason = startseason; }

    public LocalDate getEndseason() { return endseason; }

    public void setEndseason(LocalDate endseason) { this.endseason = endseason; }

    public LocalDate getCurrentday() {
        return currentday;
    }

    public void setCurrentday(LocalDate currentday) { this.currentday = currentday; }

    public List<LocalDate> getNoGameDates() { return noGameDates; }

    public void setNoGameDates(List<LocalDate> noGameDates) { this.noGameDates = noGameDates; }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getCurrentseason() {
        StringBuilder season = new StringBuilder();
        season.append(getYear())
                .append("/")
                .append(getYear()+1);
        return season.toString();
    }

    public void setCurrentseason(String currentseason) {
        this.currentseason = currentseason;
    }

    public Season(int year, LocalDate startseason, LocalDate endseason, LocalDate currentday) {
        this.year = year;
        this.startseason = startseason;
        this.endseason = endseason;
        this.currentday = currentday;
    }

    public Season(){}
}
